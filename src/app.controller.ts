import { Controller, Get } from '@nestjs/common';
//import { ConfigService } from '@nestjs/config';
import { ConfigService } from '@nestjs/config';
import { Client, ClientKafka, EventPattern, Payload } from '@nestjs/microservices';
import { AppService } from './app.service';
import { CompanyService } from './company/company.service';
import { CreateCompanyInput } from './company/dto/create-company.input';
import { Company } from './company/entities/company.entity';
import { microserviceConfig } from './kafkaConfig/microserviceConfig';
@Controller()
export class AppController {

  constructor(private readonly appService: AppService,private readonly companyService:CompanyService/*,private configService: ConfigService*/) {
  }

  @Client(microserviceConfig)
  client: ClientKafka;

  
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  onModuleInit() {
    const requestPatterns = [
      'companyServiceTopic',
    ];

    requestPatterns.forEach(pattern => {
      this.client.subscribeToResponseOf(pattern);
    });
  }
  
  @EventPattern('companyServiceTopic')
  async handleEvent(@Payload() eventMessage: any) {
    console.log("New Code- " + eventMessage);
    console.log(typeof eventMessage.value);
    console.log(JSON.parse('{"companyName":"Brightly Software -1  ","companyDescription":"Brightly Softwares India pvt. ltd.","companyCode":"Brightly Software -1 "}'));
    this.companyService.create(eventMessage.value);
    console.log('Company Created');

  }
}



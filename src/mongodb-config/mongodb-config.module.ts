import { Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongodbConfigService } from './mongodb-config';
import configurationYaml from 'config/configuratiuon';

@Global()
@Module({
    imports:[ConfigModule.forRoot({
        load: [configurationYaml]})],
    exports:[MongodbConfigService],
    providers:[MongodbConfigService,ConfigService],
})
export class MongodbConfigModule {}

import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class MongodbConfigService {

    constructor(private configService: ConfigService) {
        console.log("inside app config");
        console.log(configService.get<string>('name'));
        console.log(configService.get<string>('db.mongo.host'));
        console.log("inside app config out");
        }
    
        public async getMongoConfig() {
            console.log("Mongo Config Service");
            this.configService.get<string>('db.mongo.url')
            console.log(this.configService.get<string>('db.mongo.host')+'/'+this.configService.get<string>('db.mongo.database'));
            console.log( 'mongodb://username:password@'+ this.configService.get<string>('db.mongo.host')+'/'+this.configService.get<string>('db.mongo.database'))
            return {
                uri: this.configService.get<string>('db.mongo.url'),//'mongodb://'+ this.configService.get<string>('db.mongo.host')+'/'+this.configService.get<string>('db.mongo.database'), //'mongodb://localhost:27017/inventory',
                useNewUrlParser: true,
                useUnifiedTopology: true,
            };
        }
}

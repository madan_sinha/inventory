import { Test, TestingModule } from '@nestjs/testing';
import { CompanyService } from './company.service';
import { Model } from 'mongoose';
import { Company } from './entities/company.entity';
import { getModelToken } from '@nestjs/mongoose';
import { CreateCompanyInput } from './dto/create-company.input';

const mockCompany = {
  companyName : "Brightly",
  companyCode : "Brightly",
  companyDescription : "Brightly Software Asset Management"
}

const mockCompanies = [{
  companyName : "Brightly",
  companyCode : "Brightly",
  companyDescription : "Brightly Software Asset Management"
},{
  companyName : "Brightly Software",
  companyCode : "Dude",
  companyDescription : "Brightly Software Asset Management"
}];

describe('CompanyService', () => {
  let service: CompanyService;
  let model: Model<Company>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CompanyService, {
        provide: getModelToken('Company'), 
        useValue: {
          new: jest.fn().mockResolvedValue(mockCompany),
          constructor: jest.fn().mockResolvedValue(mockCompany),
          find: jest.fn(),
          create: jest.fn(),
          exec: jest.fn(),
          findOne: jest.fn(),
          remove: jest.fn()
        },
      },],
    }).compile();

    service = module.get<CompanyService>(CompanyService);
    model= module.get<Model<Company>>(getModelToken('Company'));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  })

  describe('create', () => {
    it('should return a company', async () => {

      const input : CreateCompanyInput = {
        companyName : "Brightly",
        companyCode : "Brightly",
        companyDescription : "Brightly Software Asset Management"
      }

      jest.spyOn(model, 'create').mockImplementationOnce(() => 
      Promise.resolve(mockCompany),
      );

      const expectedValue = await service.create(input);
      expect(expectedValue).toEqual(mockCompany);
    })
  });

  describe('findAll', () => {
    it('should return an array of companies', async () => {
      jest.spyOn(model, 'find').mockReturnValue({
          exec: jest.fn().mockResolvedValueOnce(mockCompanies), } as any);
        const expectedValue = await service.findAll();
        expect(expectedValue).toEqual(mockCompanies);
    })
  });

  describe('findOne', () => {
    it('should return an company based on code', async () => {

      jest.spyOn(model, 'findOne').mockReturnValue({
        exec: jest.fn().mockResolvedValueOnce(mockCompany), } as any);

      const expectedValue = await service.findOne("Brightly");
      expect(expectedValue).toEqual(mockCompany);
    })
  });

  describe('findOneNotFound', () => {
    it('should return an empty', async () => {

     jest.spyOn(model, 'findOne').mockReturnValue({
        exec: jest.fn().mockResolvedValueOnce(null), } as any);
      try{
        const expectedValue = await service.findOne("DudeSolution");
      }
      catch(e){
        expect(e.message).toEqual("Company Code DudeSolution not found");
      }
    })
  });
});

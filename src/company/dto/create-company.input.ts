import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class CreateCompanyInput {
  @Field(() => String)
  companyName: string;

  @Field(() => String)
  companyCode: string;

  @Field(() => String)
  companyDescription: string;

  

}

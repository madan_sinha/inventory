import { ObjectType, Field } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document, Schema as MongooseSchema } from 'mongoose';

export type CompanyDocument = Company & Document;

@Schema()
@ObjectType()
export class Company{
    
    @Field(() => String)
    @Prop()
    companyName: string;

    @Field(() => String)
    @Prop()
    companyCode: string;

    @Field(() => String)
    @Prop()
    companyDescription: string;
}

export const CompanySchema = SchemaFactory.createForClass(Company);

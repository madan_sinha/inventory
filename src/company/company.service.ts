import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { CreateCompanyInput } from './dto/create-company.input';
import { Company, CompanyDocument } from './entities/company.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class CompanyService {

  constructor(@InjectModel(Company.name) private readonly model: Model<CompanyDocument>){}

  async create(input: CreateCompanyInput):Promise<Company>{
    console.log('input ---- ' + input);
    console.log('JSON.stringify(input) ---- ' + JSON.stringify(input));

    // const createCompanyInput = new Company(input.companyCode,input.companyDescription,input.companyName);
    const company = this.model.create(input);
    console.log('input ---- ' + input);
    return company;

  }

    async findAll(){
        return this.model.find().exec();
    }

    async findOne(code: string){
        const classification  = await this.model.findOne({companyCode: code}).exec();
        
        if(!classification){
          throw new NotFoundException(`Company Code ${code} not found`)
        }
        return classification;
    }
}

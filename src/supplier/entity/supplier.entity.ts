import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document  } from 'mongoose';

@Schema()
export class Supplier extends Document{
    
    @Prop({type: String, required: true, unique: true})
    name: string;

    @Prop({type: String, required: true})
    description: string;
}

export const SupplierSchema = SchemaFactory.createForClass(Supplier);
import { Resolver } from '@nestjs/graphql';
import { SupplierService } from './supplier.service';

@Resolver()
export class SupplierResolver {
  constructor(private readonly supplierService: SupplierService) {}
}

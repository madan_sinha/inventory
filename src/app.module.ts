import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClassificationModule } from './classification/classification.module';
import { ConfigModule } from '@nestjs/config';
import configurationYaml from 'config/configuratiuon';
import { MongooseModule } from '@nestjs/mongoose';
import { MongodbConfigService } from './mongodb-config/mongodb-config';
import { MongodbConfigModule } from './mongodb-config/mongodb-config.module';
import { ItemModule } from './item/item.module';
import { SupplierModule } from './supplier/supplier.module';
import { StockModule } from './stock/stock.module';
import { GraphQLModule } from '@nestjs/graphql';
import {  ApolloFederationDriver,  ApolloFederationDriverConfig,} from '@nestjs/apollo';
import { CompanyModule } from './company/company.module';

@Module({
  imports: [
    ConfigModule,
    MongodbConfigModule,
    ClassificationModule,ConfigModule.forRoot({
    load: [configurationYaml]}),
    MongooseModule.forRootAsync({
      inject: [MongodbConfigService],
      useFactory: async (configService: MongodbConfigService) => configService.getMongoConfig(),
    }),
    ItemModule,
    SupplierModule,
    StockModule,
    GraphQLModule.forRoot<ApolloFederationDriverConfig>({
      driver: ApolloFederationDriver,
      autoSchemaFile: true,
    }),
    CompanyModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

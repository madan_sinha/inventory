import {KafkaOptions, Transport} from "@nestjs/microservices";

export const microserviceConfig: KafkaOptions = {
    transport: Transport.KAFKA,

    options: {
        client: {
            brokers: ["kafka:9092"],
        },
        consumer: {
            groupId: 'company-service-group',
            allowAutoTopicCreation: true,
        },
    }
};
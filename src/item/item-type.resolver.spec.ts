import { Test, TestingModule } from '@nestjs/testing';
import { ItemTypeResolver } from './item-type.resolver';
import { Model } from 'mongoose';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import { ItemTypeInput } from './dto/item-type.input';
import { Itemtype } from './entity/item-type.entity';
import { Classification } from '../classification/entity/classification.entity';
import { ClassificationService } from '../classification/classification.service';
import { ItemTypeService } from './item-type.service';


const mockClassification = {
  name: "test",
  description: "test",
  tag: "tag"
};

const mockItemType = {
  name: "test-item-type",
  description: "test-item-type-description",
  tag: ["test-classification tags"],
  classification:mockClassification
};

class ApiServiceMock {
  getStudent(firstName: string, lastName: string) {
    return {
      name: 'Jane Doe',
      grades: [3.7, 3.8, 3.9, 4.0, 3.6],
    };
  }
 }

describe('ItemTypeResolver', () => {
  let resolver: ItemTypeResolver;
  let model: Model<Itemtype>;
  let service: ItemTypeService;

  const ApiServiceProvider = {
    provide: ClassificationService,
    useClass: ApiServiceMock,
  };

  const itemTypes = [{
    name: "test-item-type",
    description: "test-item-type-description",
    tag: "test-classification tags",
    classification:mockClassification
  },
  {
    name: "test-item-type-1",
    description: "test-item-type-description-1",
    tag: "test-classification tags-1",
    classification:mockClassification
  }
  ];

  beforeEach(async () => {

    const module: TestingModule = await Test.createTestingModule({
      providers: [ItemTypeResolver, ApiServiceProvider,
        ItemTypeService,ClassificationService,Classification,
        {
          provide: getModelToken(Itemtype.name), 
            useValue: Model  // <-- Use the Model Class from Mongoose
        },
        {
          provide: getModelToken(Classification.name), 
            useValue: Model  // <-- Use the Model Class from Mongoose
        }],
    }).compile();

    resolver = module.get<ItemTypeResolver>(ItemTypeResolver);
    model= module.get<Model<Itemtype>>(getModelToken('Itemtype'));
      service = module.get<ItemTypeService>(ItemTypeService);
    
  });



  describe('createItemType', () => {
    it('should return a itemtype', async () => {
      const itemType = {
        name: "test",
        description: "test",
        tag: ["tag"]
      }

      const input : ItemTypeInput={
        name: "test",
        description: "test",
        tag: ["tag"]
      }

      jest.spyOn(model, 'create').mockImplementationOnce(() => 
      Promise.resolve({name: "test",
      description: "test",
      tag: ["tag"]}),
      );

      const itemTypeOutput = await resolver.createItemType(input);
      
      expect(itemTypeOutput).toEqual(itemType);
    })
  });

  describe('findAll', () => {
    it('should return an array of classifications', async () => {
      jest.spyOn(model, 'find').mockReturnValue({
        exec: jest.fn().mockResolvedValueOnce(itemTypes), } as any);
      const itemTypeOutput = await resolver.findAll();
      expect(itemTypeOutput).toEqual(itemTypes);
    })
  });

  describe('findOne', () => {
      it('should return an item Type', async () => {
        const itemType = {
          name: "test",
          description: "test",
          tag: ["tag"]
        }
  
        jest.spyOn(model, 'findOne').mockReturnValue({
          exec: jest.fn().mockResolvedValueOnce(itemType), } as any);
  
        const itemTypeOutput = await resolver.findOne("TestId");
        expect(itemTypeOutput).toEqual(itemType);
    })
  });

  describe('removeItemType', () => {
    it('should return an id of removed classification', async () => {
      const itemTypeName = "item-type-name";
            const itemType = {
              name: "item-type-name",
              description: "test",
              tag: ["tag"]
            }
    
            jest.spyOn(model, 'findOne').mockReturnValue({
              exec: jest.fn().mockResolvedValueOnce({name: "item-type-name",
              description: "test",
              tag: ["tag"]}), } as any);
            
            model.deleteOne = jest.fn().mockReturnValueOnce({name: "test",
            description: "test",
            tag: "tag"});
    
            const itemTypeNameOutput = await resolver.removeItemType(itemTypeName);
            expect(itemTypeNameOutput).toEqual(itemType);
      })
  });

  describe('classifications', () => {
    it('Returns classification based on item-type', async () => {
      const itemTypeName = "item-type-name";
            const itemtype :Itemtype ={
              name: "item-type-name",
              description: "test",
              classificationTags: ["tag"],
              classifications:[ ]

            }


              const mockClassifications = [{
                name: "test",
                description: "test",
                tag: "tag"
              },
              {
                name: "test1",
                description: "test1",
                tag: "tag1"
              }
              ];
            
            ClassificationService.prototype.findAllByTags = jest.fn().mockReturnValueOnce(mockClassifications );
              
            const itemTypeNameOutput = await resolver.classifications(itemtype);
            expect(itemTypeNameOutput).toEqual(mockClassifications);
      })
  });
});

import { Test, TestingModule } from '@nestjs/testing';
import { ItemTypeService } from './item-type.service';
import { Itemtype } from './entity/item-type.entity';
import { Model } from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';
import { ItemTypeInput } from './dto/item-type.input';
import { Classification } from '../classification/entity/classification.entity';

const mockClassification = {
  name: "test",
  description: "test",
  tag: "tag"
};

const mockItemType = {
  name: "test-item-type",
  description: "test-item-type-description",
  tag: ["test-classification tags"],
  classification:mockClassification
};


describe('ItemTypeService', () => {
  let service: ItemTypeService;
  let model: Model<Itemtype>;



  const itemTypes = [{
    name: "test-item-type",
    description: "test-item-type-description",
    tag: "test-classification tags",
    classification:mockClassification
  },
  {
    name: "test-item-type-1",
    description: "test-item-type-description-1",
    tag: "test-classification tags-1",
    classification:mockClassification
  }
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ItemTypeService,
        {
        provide: getModelToken(Itemtype.name), 
          useValue: Model  // <-- Use the Model Class from Mongoose
      },{
        provide: getModelToken(Classification.name), 
        useValue: Model  // <-- Use the Model Class from Mongoose
      }
      ],
    }).overrideProvider(Classification).useValue(Model).compile();

    
    service = module.get<ItemTypeService>(ItemTypeService);
    model= module.get<Model<Itemtype>>(getModelToken('Itemtype'));
  
  });

  describe('create', () => {
    it('should return an itemType', async () => {
      const itemType = {
        name: "test",
        description: "test",
        tag: ["tag"]
      }

      const input : ItemTypeInput = {
        name: "test",
        description: "test",
        tag: ["tag"]
      }

      jest.spyOn(model, 'create').mockImplementationOnce(() => 
      Promise.resolve({name: "test",
      description: "test",
      tag: ["tag"]}),
      );

      const itemTypeOutput = await service.create(input);
      
      expect(itemTypeOutput).toEqual(itemType);
    })
    });

    describe('findAll', () => {
      it('should return an array of item-types', async () => {
        jest.spyOn(model, 'find').mockReturnValue({
            exec: jest.fn().mockResolvedValueOnce(itemTypes), } as any);
          const itemTypeOutput = await service.findAll();
          expect(itemTypeOutput).toEqual(itemTypes);
        })
      });

      describe('findOne', () => {
        it('should return an item Type', async () => {
          const itemType = {
            name: "test",
            description: "test",
            tag: ["tag"]
          }
    
          jest.spyOn(model, 'findOne').mockReturnValue({
            exec: jest.fn().mockResolvedValueOnce(itemType), } as any);
    
          const itemTypeOutput = await service.findOne("TestId");
          expect(itemTypeOutput).toEqual(itemType);
        })
      });


    describe('remove', () => {
      it('should return the removed item-type', async () => {
          const itemTypeName = "item-type-name";
          const itemType = {
            name: "item-type-name",
            description: "test",
            tag: ["tag"]
          }
  
          jest.spyOn(model, 'findOne').mockReturnValue({
            exec: jest.fn().mockResolvedValueOnce({name: "item-type-name",
            description: "test",
            tag: ["tag"]}), } as any);
          
          model.deleteOne = jest.fn().mockReturnValueOnce({name: "test",
          description: "test",
          tag: "tag"});
  
          const itemTypeNameOutput = await service.remove(itemTypeName);
          expect(itemTypeNameOutput).toEqual(itemType);
        })
      });

    describe('findOneNotFound', () => {
      it('should return an exception', async () => {
        const itemType = {
          name: "test",
          description: "test",
          tag: ["tag"]
        }
  
        jest.spyOn(model, 'findOne').mockReturnValue({
          exec: jest.fn().mockResolvedValueOnce(null), } as any);
        try{
        const itemTypeOutput = await service.findOne("TestId");
        }
        catch(e){
          expect(e.message).toEqual("Name TestId not found");
        }
      })
    });

    describe('removeNotFound', () => {
      it('should return the not found exception', async () => {
          const itemTypeName = "item-type-name";
          const itemType = {
            name: "item-type-name",
            description: "test",
            tag: ["tag"]
          }
  
          jest.spyOn(model, 'findOne').mockReturnValue({
            exec: jest.fn().mockResolvedValueOnce(null), } as any);
          
          model.deleteOne = jest.fn().mockReturnValueOnce({name: "test",
          description: "test",
          tag: "tag"});
          try{
            const itemTypeNameOutput = await service.remove(itemTypeName);
          }
          catch(e){
            expect(e.message).toEqual("Name item-type-name not found");
          }
        })
      });
});


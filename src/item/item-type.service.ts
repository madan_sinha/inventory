import { Injectable, NotFoundException } from '@nestjs/common';
import { Parent, ResolveField } from '@nestjs/graphql';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ItemTypeInput } from './dto/item-type.input';
import { Itemtype } from './entity/item-type.entity';

@Injectable()
export class ItemTypeService {

    constructor(@InjectModel(Itemtype.name) private readonly model: Model<Itemtype>){}

    async create(input: ItemTypeInput){
            
        const itemType = await this.model.create({ name: input.name,
          description: input.description,
          classificationTags:input.tag});
        return itemType;
    }

    async findAll(){
      let itemType =[];
        itemType = await this.model.find().exec();
       // console.log(itemType[0].toJSON());
        return itemType;
    }

    async findOne(name: string){
        const itemType = await this.model.findOne({name: name}).exec();
        
        if(!itemType){
          throw new NotFoundException(`Name ${name} not found`);
        }
        return itemType;
    }

    async remove(name: string){
        const itemType = await this.model.findOne({name: name}).exec();

        if(!itemType){
          throw new NotFoundException(`Name ${name} not found`);
        }
        else{
          await this.model.deleteOne({name: name});
        }
        return itemType;
    }
}

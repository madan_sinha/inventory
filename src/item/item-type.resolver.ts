import { Args, Mutation, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { ClassificationService } from '../classification/classification.service';
import { Classification } from '../classification/entity/classification.entity';
import { ItemTypeInput } from './dto/item-type.input';
import { Itemtype } from './entity/item-type.entity';
import { ItemTypeService } from './item-type.service';

@Resolver(() => Itemtype)
export class ItemTypeResolver {
  constructor(private readonly itemTypeService: ItemTypeService, private readonly classificationService: ClassificationService) {}

  @Mutation(() => Itemtype)
  createItemType(@Args('itemTypeInput') input: ItemTypeInput){
    return this.itemTypeService.create(input);
  }

  @Query(() => [Itemtype], {name: 'itemtypes'})
  findAll(){
    return this.itemTypeService.findAll();
  }

  @ResolveField((of) => Classification)
  async classifications(@Parent() itemType: Itemtype) {
    return this.classificationService.findAllByTags(itemType.classificationTags);
  }

  @Query(() => Itemtype, { name: 'itemtype'})
  findOne(@Args('name', {type:() => String}) name: string){
    return this.itemTypeService.findOne(name);
  }

  @Mutation(() => Itemtype)
  removeItemType(@Args('name', {type: () => String}) name: string){
    return this.itemTypeService.remove(name);
  }
}

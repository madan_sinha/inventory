import { ObjectType, Field, ID } from '@nestjs/graphql';

@ObjectType()
export class ItemTypeModel{
    
    @Field(() => String)
    name: string;

    @Field(() => String)
    description: string;

    @Field(() => [String])
    classificationTags: string[];
}

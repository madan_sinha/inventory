import { InputType, Field } from "@nestjs/graphql";

@InputType()
export class ItemInput {

  @Field(() => String)
  code: string;

  @Field(() => String)
  category: string;

  @Field(() => String)
  companyId: string;

  @Field(() => String)
  stockId: string;

  @Field(() => String)
  itemTypeId: string;

  @Field(() => String)
  name: string;

  @Field(() => String)
  description: string;

  @Field(() => String)
  media: string;
}
import { Module } from '@nestjs/common';
import { ItemService } from './item.service';
import { ItemResolver } from './item.resolver';
import { MongooseModule } from '@nestjs/mongoose';
import { Itemtype, ItemtypeSchema } from './entity/item-type.entity';
import { ItemTypeResolver } from './item-type.resolver';
import { ItemTypeService } from './item-type.service';
import { Classification, ClassificationSchema } from '..//classification/entity/classification.entity';
import { ClassificationService } from '../classification/classification.service';
import { ClassificationResolver } from '../classification/classification.resolver';
import { ClassificationModule } from '../classification/classification.module';

@Module({
  providers: [ItemResolver, ItemService, ItemTypeResolver, ItemTypeService, ClassificationService, ClassificationResolver],
  imports: [ClassificationModule,/*GraphQLModule.forRoot<ApolloFederationDriverConfig>({
    driver: ApolloFederationDriver,
    autoSchemaFile: true,
    buildSchemaOptions: {
      orphanedTypes: [Classification],
    }
  }),*/MongooseModule.forFeature([{name: Itemtype.name, schema: ItemtypeSchema},{ name: Classification.name, schema: ClassificationSchema }])]
})
export class ItemModule {}

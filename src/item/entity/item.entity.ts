import { Schema, Prop, SchemaFactory } from "@nestjs/mongoose";
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Itemtype } from "./item-type.entity";

@Schema()
export class Item extends Document{

    @Prop({ type: String, required: true })
    code: string;

    @Prop({ type: String, required: true })
    category: string;

    @Prop({ type: MongooseSchema.Types.ObjectId, required: false, ref: Itemtype.name })
    itemType: MongooseSchema.Types.ObjectId;
}

export const ItemSchema = SchemaFactory.createForClass(Item);
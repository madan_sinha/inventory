import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document} from 'mongoose';
import * as mongoose from 'mongoose';
import { Classification } from '../../classification/entity/classification.entity';
import { ObjectType, Field } from '@nestjs/graphql';

export type ItemtypeDocument = Itemtype & Document;

@Schema()
@ObjectType()
export class Itemtype{
    
    @Field(() => String)
    @Prop({type: String, required: true})
    name: string;

    @Field(() => String)
    @Prop({type: String, required: true})
    description: string;

    @Field(() => [String])
    @Prop([String])
    classificationTags: string[];

    @Field((type) => [Classification])
    //@Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Classification' }] })
    classifications: Classification[];
}

export const ItemtypeSchema = SchemaFactory.createForClass(Itemtype);
import { Schema, Prop, SchemaFactory } from "@nestjs/mongoose";
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Item } from "./item.entity";

@Schema()
export class ItemDetails extends Document{

    @Prop({ type: String, required: true })
    name: string;

    @Prop({ type: String, required: true })
    description: string;

    @Prop({type: Number})
    quantityAvailable: Number

    @Prop({type: Number})
    reservedQuantity: Number

    @Prop({type: MongooseSchema.Types.ObjectId, required: true, ref: Item.name})
    item: MongooseSchema.Types.ObjectId;
}

export const ItemDetailsSchema = SchemaFactory.createForClass(ItemDetails);
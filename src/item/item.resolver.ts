import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ItemTypeInput } from './dto/item-type.input';
import { Itemtype } from './entity/item-type.entity';
import { Item } from './entity/item.entity';
import { ItemService } from './item.service';

@Resolver(() => Item)
export class ItemResolver {
  constructor(private readonly itemService: ItemService) {}

}

import { InputType, Field } from "@nestjs/graphql";

@InputType()
export class ClassificationInput {

  @Field(() => String)
  name: string;

  @Field(() => String)
  description: string;

  @Field(() => String)
  tag: string;
}
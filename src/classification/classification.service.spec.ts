import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { ClassificationService } from './classification.service';
import { Classification } from './entity/classification.entity';
import { Model } from 'mongoose';
import { ClassificationInput } from './dto/classification.input';
import { NotFoundException } from '@nestjs/common';

const mockClassification = {
  name: "test",
  description: "test",
  tag: "tag"
};

describe('ClassificationService', () => {
  
  let service: ClassificationService ;
  let model: Model<Classification>;

  const classifications = [{
    name: "test",
    description: "test",
    tag: "tag"
  },
  {
    name: "test1",
    description: "test1",
    tag: "tag1"
  }
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassificationService,
        {
          provide: getModelToken('Classification'), 
          useValue: {
            new: jest.fn().mockResolvedValue(mockClassification),
            constructor: jest.fn().mockResolvedValue(mockClassification),
            find: jest.fn(),
            create: jest.fn(),
            exec: jest.fn(),
            findOne: jest.fn(),
            remove: jest.fn()
          },
        },
      ],
    }).compile();

    service = module.get<ClassificationService>(ClassificationService);
    model= module.get<Model<Classification>>(getModelToken('Classification'));
  });

  describe('create', () => {
    it('should return a classification', async () => {
      const classification = {
        name: "test",
        description: "test",
        tag: "tag"
      }

      const input : ClassificationInput={
        name: "test",
        description: "test",
        tag: "tag"
      }

      jest.spyOn(model, 'create').mockImplementationOnce(() => 
      Promise.resolve({name: "test",
      description: "test",
      tag: "tag"}),
      );

      const classificationOrg = await service.create(input);
      expect(classificationOrg).toEqual(classification);
    })
    });

  describe('findAll', () => {
    it('should return an array of classifications', async () => {
      jest.spyOn(model, 'find').mockReturnValue({
          exec: jest.fn().mockResolvedValueOnce(classifications), } as any);
        const classificationOrg = await service.findAll();
        expect(classificationOrg).toEqual(classifications);
      })
    });

  describe('findOne', () => {
    it('should return an classification', async () => {
      const classification = {
        name: "test",
        description: "test",
        tag:"test"
      }

      jest.spyOn(model, 'findOne').mockReturnValue({
        exec: jest.fn().mockResolvedValueOnce(classification), } as any);

      const classificationOrg = await service.findOne("TestId");
      expect(classificationOrg).toEqual(classification);
    })
  });

  describe('remove', () => {
    it('should return an id of removed classification', async () => {
        const classificationTag = "tag";

        jest.spyOn(model, 'findOne').mockReturnValue({
          exec: jest.fn().mockResolvedValueOnce({name: "test",
          description: "test",
          tag: "tag"}), } as any);
        
        model.deleteOne = jest.fn().mockReturnValueOnce({name: "test",
        description: "test",
        tag: "tag"});

        const classificationIdReturned = await service.remove(classificationTag);
        expect(classificationIdReturned).toEqual(mockClassification);
      })
    });

    describe('findOneNotFound', () => {
      it('should return an empty', async () => {
        
        const t = () => {
          throw new NotFoundException();
        };

        const classification = null;
  
       jest.spyOn(model, 'findOne').mockReturnValue({
          exec: jest.fn().mockResolvedValueOnce(classification), } as any);
        try{
        const classificationOrg = await service.findOne("TestIdInvalid");
        
        }
        catch(e){
          expect(e.message).toEqual("Tag TestIdInvalid not found");
        }
      })
    });

    describe('removeNotFound', () => {
      it('should return an error', async () => {
          const classificationTag = "tag";
          const classification = null; 
          jest.spyOn(model, 'findOne').mockReturnValue({
            exec: jest.fn().mockResolvedValueOnce(classification), } as any);
          
        
          model.deleteOne = jest.fn().mockReturnValueOnce(classification);
  
          try{
            const classificationIdReturned = await service.remove(classificationTag);
            }
            catch(e){
              expect(e.message).toEqual("Tag tag not found");
            }
        })
      });

      describe('findOfindAllByTags', () => {
        it('should return  classifications', async () => {

          jest.spyOn(model, 'find').mockReturnValue({
            exec: jest.fn().mockResolvedValueOnce(classifications), } as any);
    
          const classificationOrg = await service.findAllByTags(["TestId","Test"]);
          expect(classificationOrg).toEqual(classifications);
        })
      });  
});

import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document, Schema as MongooseSchema } from 'mongoose';
import { ObjectType, Field } from '@nestjs/graphql';


export type ClassificationDocument = Classification & Document;

@Schema()
@ObjectType()
export class Classification{
    
    @Field(() => String)
    @Prop({ required: true})
    name: string;

    @Field(() => String)
    @Prop({ required: true })
    description: string;

    @Field(() => String)
    @Prop({ required: true, unique: true })
    tag: string;
}

export const ClassificationSchema = SchemaFactory.createForClass(Classification);

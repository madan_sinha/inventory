import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ClassificationService } from './classification.service';
import { ClassificationInput } from './dto/classification.input';
import { Classification } from './entity/classification.entity';

@Resolver(() => Classification)
export class ClassificationResolver {
  constructor(private readonly classificationService: ClassificationService) {}

  @Mutation(() => Classification)
  createClassification(@Args('classificationInput') classificationInput: ClassificationInput){
    return this.classificationService.create(classificationInput);
  }

  @Query(() => [Classification], {name: 'classifications'})
  findAll(){
    return this.classificationService.findAll();
  }

  @Query(() => Classification, { name: 'classification'})
  findOne(@Args('tag', {type:() => String}) tag: string){
    return this.classificationService.findOne(tag);
  }

  @Mutation(() => Classification)
  removeClassification(@Args('tag', {type: () => String}) tag: string){
    return this.classificationService.remove(tag);
  }
}

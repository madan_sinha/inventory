import { Test, TestingModule } from '@nestjs/testing';
import { ClassificationResolver } from './classification.resolver';
import { ClassificationService } from './classification.service';
import { Classification } from './entity/classification.entity';
import { Model } from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';
import { ClassificationInput } from './dto/classification.input';

const mockClassification = {
  name: "test",
  description: "test",
  tag: "tag"
};

describe('ClassificationResolver', () => {
  let resolver: ClassificationResolver;
  let model: Model<Classification>;
  const classifications = [{
    name: "test",
    description: "test",
    tag: "tag"
  },
  {
    name: "test1",
    description: "test1",
    tag: "tag1"
  }
  ];
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClassificationResolver, ClassificationService,
        {
          provide: getModelToken('Classification'), 
          useValue: {
            new: jest.fn().mockResolvedValue(mockClassification),
            constructor: jest.fn().mockResolvedValue(mockClassification),
            find: jest.fn(),
            create: jest.fn(),
            exec: jest.fn(),
            findOne: jest.fn(),
            remove: jest.fn()
          },
        },],
    }).compile();

    resolver = module.get<ClassificationResolver>(ClassificationResolver);
    model= module.get<Model<Classification>>(getModelToken('Classification'));
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });

  describe('createClassification', () => {
    it('should return a classification', async () => {
      const classification = {
        name: "test",
        description: "test",
        tag: "tag"
      }

      const input : ClassificationInput ={
        name: "test",
        description: "test",
        tag: "tag"
      }

      jest.spyOn(model, 'create').mockImplementationOnce(() => 
      Promise.resolve({name: "test",
      description: "test",
      tag: "tag"}),
      );

      const classificationOrg = await resolver.createClassification(input);
      expect(classificationOrg).toEqual(classification);
    })
  });

  describe('findAll', () => {
    it('should return an array of classifications', async () => {
      jest.spyOn(model, 'find').mockReturnValue({
          exec: jest.fn().mockResolvedValueOnce(classifications), } as any);
        const classificationOrg = await resolver.findAll();
        expect(classificationOrg).toEqual(classifications);
      })
    });

  describe('findOne', () => {
    it('should return an classification', async () => {
      const classification = {
        name: "test",
        description: "test",
        tag:"test"
      }

      jest.spyOn(model, 'findOne').mockReturnValue({
        exec: jest.fn().mockResolvedValueOnce(classification), } as any);

      const classificationOrg = await resolver.findOne("TestId");
      expect(classificationOrg).toEqual(classification);
    })
  });

  describe('remove', () => {
    it('should return an id of removed classification', async () => {
        const classificationTag = "tag";

        jest.spyOn(model, 'findOne').mockReturnValue({
          exec: jest.fn().mockResolvedValueOnce({name: "test",
          description: "test",
          tag: "tag"}), } as any);
        
        model.deleteOne = jest.fn().mockReturnValueOnce({name: "test",
        description: "test",
        tag: "tag"});

        const classificationIdReturned = await resolver.removeClassification(classificationTag);
        expect(classificationIdReturned).toEqual(mockClassification);
      })
  });

});

import { Module } from '@nestjs/common';
import { ClassificationService } from './classification.service';
import { ClassificationResolver } from './classification.resolver';
import { MongooseModule } from '@nestjs/mongoose';
import { Classification, ClassificationSchema } from './entity/classification.entity';

@Module({
  providers: [ClassificationResolver, ClassificationService],
  imports: [
  MongooseModule.forFeature([{ name: Classification.name, schema: ClassificationSchema }]),
],
})
export class ClassificationModule {}

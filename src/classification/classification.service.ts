import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Classification } from './entity/classification.entity';
import { Model } from 'mongoose';
import { ClassificationInput } from './dto/classification.input';

@Injectable()
export class ClassificationService {

    constructor(@InjectModel(Classification.name) private readonly classificationModel: Model<Classification>){}

    async create(input: ClassificationInput){
        const classification = await this.classificationModel.create(input);
        return classification;
    }

    async findAll(){
        return this.classificationModel.find().exec();
    }

    async findOne(tag: string){
        const classification  = await this.classificationModel.findOne({tag: tag}).exec();
        
        if(!classification){
          throw new NotFoundException(`Tag ${tag} not found`)
        }
        return classification;
    }

    async remove(tag: string){
        const classification = await this.classificationModel.findOne({tag: tag}).exec();

        if(!classification){
          throw new NotFoundException(`Tag ${tag} not found`);
        }
        else{
          await this.classificationModel.deleteOne({tag: tag});
        }
        return classification;
    }

    async findAllByTags(tags: string[]){
     return this.classificationModel.find({tag:tags}).exec();
  }
}

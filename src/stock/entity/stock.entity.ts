import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document} from 'mongoose';
import { ObjectType, Field, ID } from '@nestjs/graphql';

export type StockDocument = Stock & Document;

@Schema()
@ObjectType()
export class Stock{
    
    @Field(() => Number)
    @Prop({type: Number, required: true})
    availableQty: number;

    @Field(() => String)
    @Prop({type: String, required: true})
    locationCode: string;
}

export const StockSchema = SchemaFactory.createForClass(Stock);
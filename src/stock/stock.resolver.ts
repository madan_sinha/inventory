import { Resolver } from '@nestjs/graphql';
import { StockService } from './stock.service';

@Resolver()
export class StockResolver {
  constructor(private readonly stockService: StockService) {}
}
